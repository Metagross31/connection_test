use shared::{Struct2};
use tokio::net::TcpListener;

#[tokio::main]
async fn main() {
    // Address to bind and listen for incoming packets
    let own_address = "127.0.0.1:8001";

    let listener = TcpListener::bind(own_address).await.unwrap();
    let (mut stream, _) = listener.accept().await.unwrap();

    loop {
        let x = match shared::read_struct_from_stream(&mut stream).await {
            Ok(x)=> x,
            Err(e) => {
                println!("Received error \"{e}\", terminating.");
                break;
            }
        };

        let y = Struct2::calculate(&x);
        println!("Sending back the response {:?}.", y);

        let mut i = 0;
        while let Err(e) = shared::send_struct_through_stream(&y, &mut stream).await {
            println!("Error: {e}.");
            i += 1;
            if i > 5 {
                println!("Too many tries!");
                break;
            }
            println!("Trying again...");
        }
    }
}
