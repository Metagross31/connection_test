use rand::Rng;
use serde::{Deserialize, Serialize};
use std::thread;
use std::time::Duration;
use serde::de::DeserializeOwned;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;

#[derive(Clone, Debug, Default, Hash, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub struct Struct1(i32);

#[derive(Clone, Debug, Default, Hash, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub struct Struct2(i32);

impl Struct1 {
    pub fn calculate(x: i32) -> Self {
        let secs = rand::thread_rng().gen_range(0..2);
        thread::sleep(Duration::from_secs(secs));
        Self(2 * x)
    }
}

impl Struct2 {
    pub fn calculate(s1: &Struct1) -> Self {
        let secs = rand::thread_rng().gen_range(10..20);
        thread::sleep(Duration::from_secs(secs));
        Self(s1.0 * s1.0)
    }
}

pub async fn send_struct_through_stream<T>(
    x: &T,
    stream: &mut TcpStream,
) -> Result<(), Box<dyn std::error::Error>>
where
    T: Serialize,
{
    let x = bincode::serialize(x)?;
    stream.write_u32_le(x.len().try_into()?).await?;
    stream.write_all(&x).await?;
    Ok(())
}

pub async fn read_struct_from_stream<T>(
    stream: &mut TcpStream,
) -> Result<T, Box<dyn std::error::Error>>
where
    T: DeserializeOwned,
{
    let size = stream.read_u32_le().await?;
    let mut y = vec![0; size as usize];
    stream.read_exact(&mut y).await?;
    let y: T = bincode::deserialize(&y)?;
    Ok(y)
}
