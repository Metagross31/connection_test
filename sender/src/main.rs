use shared::{Struct1, Struct2};
use tokio::net::TcpStream;

#[tokio::main]
async fn main() {
    let ip_addresses = ["127.0.0.1:8001"];

    let mut responses = vec![];

    let mut streams = vec![];
    for address in ip_addresses {
        let stream = TcpStream::connect(address).await.unwrap();
        streams.push(stream);
    }

    let possible_inputs = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    for input in possible_inputs {
        let x = Struct1::calculate(input);

        for (j, stream) in streams.iter_mut().enumerate() {
            println!("Sending {:?} to {}.", x, ip_addresses[j]);
            let mut i = 0;
            while let Err(e) = shared::send_struct_through_stream(&x, stream).await {
                println!("Error: {e}.");
                i += 1;
                if i > 5 {
                    println!("Too many tries!");
                    break;
                }
                println!("Trying again...");
            }
            let y: Struct2 = shared::read_struct_from_stream(stream).await.unwrap();
            responses.push(y);
        }
    }

    println!("Responses:\n{:?}", responses);
    println!("Now I'm happy :)");
}
